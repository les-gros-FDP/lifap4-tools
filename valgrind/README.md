# Fichier de suppressions d'erreur pour Valgrind

Valgrind est un outil d'analyse de l'utilisation de la mémoire de votre programme, ce qui est extrement pratique.
Pour celle et ceux qui utiliseront SDL2 pour leur programme, il faut savoir que SDL2 contient des "fuites mémoires" "normales", et que celle-ci ne sont pas corrigeable, à moins de re-programmer la SDL2.
Pour éviter que celles-ci ne viennent vous perturber lorsque vous essayez d'analyser votre programme, il faut utiliser un fichier de suppression qui empêchera les erreurs SDL d'apparaître.


# /!\  TRÈS IMPORTANT  /!\  

En fonction de votre carte graphique, ces erreurs changent et sont très probablement différentes que celles qu'on a eu sur les machines que l'on a utilisé pour notre projet. Il est important de savoir les détécter et les rajouter dans ce fichier ! (vous pouvez même faire une Merge Request sur ce dépot avec les fichiers de log pour comparer vos suppressions d'erreur pour qu'on puisse le mettre à jour du mieux possible !)

# Utilisation

Pour se servir de valgrind avec le fichier de suppression, il faut lancer la commande suivante :

`valgrind --tool=memcheck --suppressions=./doc/SDL.supp -s --leak-check=full ./bin/test`

Ou, si vous utilisez la cible Makefile fourni dans ce dépot :

`make debug`

Pensez à changer les chemins/nom de fichier en fonction de votre projet.

## Rajouter des erreurs dans le fichier de suppression

Si vous détectez une erreur dû à la SDL2 (lié à l'affichage donc), vous pouvez générer les suppressions à rajouter dans le fichier avec la commande suivante :

`valgrind --tool=memcheck --suppressions=./doc/valgrind_lif7.supp -s --leak-check=full --gen-suppressions=all ./bin/test`
