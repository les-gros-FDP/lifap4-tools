# Extrait de mon makefile de projet LIFAP4 :

Permet d'automatiser certaines commandes/actions pour générer la documentation/UML et le debuggage/analyse mémoire de votre programme.

Paramètres que vous devrez peut-être changer :
Le chemin (PATH) de votre fichier de configuration Doxygen et de votre binaire (executable) de test (regression).

## Version simplifié :
```Makefile
.PHONY: docs
docs: 
        @doxygen ./doc/image.doxy

.PHONY: debug
debug: all 
        @printf '\n\e[1;34m Executing GDB on the binaries... \e[0m \n\n'
        gdb -ex run ./bin/test -ex quit || gdb -ex start ./bin/test
        @printf '\n\e[1;34m Executing valgrind on the binaries... \e[0m \n\n'
        valgrind --tool=memcheck --suppressions=./doc/valgrind_lif7.supp -s --leak-check=full ./bin/test
```


## Version complète :

La version si dessous permet de lancer automatiquement votre navigateur web avec votre documentation mise à jour. Ainsi que la votre binaire de test avec et sans affichage graphique si vous en avez.

```Makefile
.PHONY: docs
docs: 
        @doxygen ./doc/image.doxy && firefox -new-window ./doc/html/index.html

.PHONY: debug
debug: all 
        @printf '\n\e[1;34m Executing GDB on the binaries... \e[0m \n\n'
        gdb -ex run ./bin/test -ex quit || gdb -ex start ./bin/test
        @printf '\n\e[1;34m Executing valgrind on the binaries... \e[0m \n\n'
        valgrind --tool=memcheck --suppressions=./doc/valgrind_lif7.supp -s --leak-check=full ./bin/test

.PHONY: debug-display
debug-display: all 
        @printf '\n\e[1;34m Executing GDB on the binaries without display... \e[0m \n\n'
        gdb -ex run ./bin/test -ex quit || gdb -ex start ./bin/test
        @printf '\n\e[1;34m Executing valgrind on the binaries with display... \e[0m \n\n'
        valgrind --tool=memcheck --suppressions=./doc/valgrind_lif7.supp -s --leak-check=full ./bin/test -d
```
