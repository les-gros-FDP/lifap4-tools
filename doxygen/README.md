# Doxygen

Fichier de configuration pour doxygen permettant de générer
automatiquement la documentation et les UMLS en fonction de votre code C++.

Il ne faut pas oublier de correctement renommer votre projet, ainsi que d'installer les dépendances nécessaires : `graphviz`.

# Utilisation

- placer le doxyfile (image.doxy) dans le répertoire de votre documentation
- lancer la commande `doxygen ./.../image.doxy` ou `make docs` si vous utilisez les cibles makefile fournies dans ce dépot.
